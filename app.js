const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const querystring = require('querystring')
const jsdom = require("jsdom")
const { JSDOM } = jsdom

const app = express()

app.use(bodyParser.json())

app.get('/etymonline*', (req, res) => {
  const query = Object.assign({}, req.query)
  let callback = null
  if (callback = query.callback) {
    delete query.callback
  }
  const queryStr = querystring.stringify(query)

  requestAsync({
    hostname: 'www.etymonline.com',
    path: '/index.php?' + queryStr,
    method: 'GET',
  })
  .then(text => {
    let terms = null
    try {
      terms = parseTerms(text)
    } catch (e) {
      throw e
    }

    if (callback) {
      res.jsonp(terms)
    } else {
      res.json(terms)
    }
  })
  .catch(result => {
    console.log(result)
    res.status(500).send(result + '')
  })

})

function parseTerms (text) {
  const dom = new JSDOM(text)
  const document = dom.window.document
  const dts = [...document.querySelectorAll('dt')].map(dt => dt.textContent)
  const dds = [...document.querySelectorAll('dd')].map(dd => dd.textContent)
  return {
    dts,
    dds
  }
}

app.listen(17813, () => {
  console.log('listening on port 17813!')
})

function requestAsync (opt, encoding) {
  return new Promise((resolve, reject) => {
    let req = http.request(opt,function (res){
      let data = ''
      res.setEncoding(encoding || 'utf8')
      res.on('data', chunk => data += chunk)
      res.on('end', () => resolve(data))
    })
    req.on('error', e => reject(e))
    req.end()
  })
}
